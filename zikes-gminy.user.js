// ==UserScript==
// @name         Gminy Zikes
// @namespace    globalbus
// @version      7
// @description  Modify zikes map
// @author       globalbus
// @match        *://zikes.website/*
// @match        https://zikes-web-client.appspot.com*
// @require https://cdnjs.cloudflare.com/ajax/libs/leaflet-providers/1.1.17/leaflet-providers.js
// @require https://cdnjs.cloudflare.com/ajax/libs/leaflet-hash/0.2.1/leaflet-hash.js
// @require https://photo.globalbus.info/js/Leaflet.VectorTile.js
// @run-at document-end
// @downloadURL https://bitbucket.org/globalbus/zikes-gminy/downloads/zikes-gminy.user.js
// ==/UserScript==

(function () {
    'use strict';
    const defaultStyle = {
        color: "#e88127",
        weight: 1,
        opacity: 0.8,
        fillOpacity: 0.1,
        fillColor: "#e88127"
    };
    const visitedStyle = {
        color: "#93a432",
        weight: 1,
        opacity: 0.8,
        fillOpacity: 0.5,
        fillColor: "#93a432"
    };
    const emptyStyle = {
        color: '#3388ff',
        weight: 1,
        opacity: 0.8,
        fillOpacity: 0,
        fillColor: '*'
    };

    function getState() {
        const storedData = localStorage.getItem('gminy');
        if (storedData) {
            return JSON.parse(storedData);
        }
        return {
            gminyPolygons: {},
            enabled: false
        };
    }

    const state = getState();
    let mapsPlaceholder;
    const parser = function (data) {
        return [data];
    };
    const switchState = function (context) {
        const el = context.feature.properties.id;
        if (state.gminyPolygons[el] === undefined)
            state.gminyPolygons[el] = {};
        if (state.gminyPolygons[el].selected === 'mark') {
            context.setStyle(emptyStyle);
            state.gminyPolygons[el].selected = false;
        }
        else if (!state.gminyPolygons[el].selected) {
            context.setStyle(defaultStyle);
            state.gminyPolygons[el].selected = 'mark';
        }
    };
    const onEach = function (feature, layer) {
        if (feature.properties) {
            layer._openTooltip = function (e) {
                if (!this._tooltip || !this._map || !e.originalEvent.ctrlKey) {
                    return;
                }
                this.openTooltip(layer, e.latlng);
            };
            layer.bindTooltip(feature.properties.name, {sticky: true});
            if (mapsPlaceholder) {
                const event = mapsPlaceholder._events.singleclick[0];
                const originalFn = event.fn;
                event.fn = function (e) {
                    if (!e.originalEvent.ctrlKey) {
                        originalFn(e);
                    }
                };
                const pane = mapsPlaceholder.createPane("betweenOverlayAndVectors");
                pane.style.zIndex = 250;
                mapsPlaceholder = null;
            }
            layer.options.pane = "betweenOverlayAndVectors";
            const el = feature.properties.id;
            layer.on('click', function (e) {
                if (e.originalEvent.ctrlKey) {
                    switchState(this);
                }
            });
            if (state.gminyPolygons[el] === undefined)
                state.gminyPolygons[el] = {name: feature.properties.name};
            if (state.gminyPolygons[el].selected === 'track')
                layer.setStyle(visitedStyle);
            else if (state.gminyPolygons[el].selected === 'mark')
                layer.setStyle(defaultStyle);
            else
                layer.setStyle(emptyStyle);
        }
    };
    const gminy = L.vectorTile({
        url: 'https://photo.globalbus.info/zikes/tile/{z}/{x}/{y}/',
        //url: 'http://localhost:8080/zikes/tile/{z}/{x}/{y}/',
        minZoom: 1,
        maxZoom: 16,
        parser: parser,
        onEachFeature: onEach
    });
    const parseData = function (trackInfo) {
        for (let j = 0; j < trackInfo.gminyId.length; j++) {
            const el = trackInfo.gminyId[j];
            if (state.gminyPolygons[el] === undefined)
                state.gminyPolygons[el] = {};
            state.gminyPolygons[el].selected = 'track';
            gminy.setStyle(visitedStyle, el);
        }
    };
    if (!localStorage.getItem('zikesMessages'))
        localStorage.setItem('zikesMessages', '{"welcome":true}');
    const auth = localStorage.getItem("auth.js");
    if (auth && JSON.parse(auth).userDetails.id === '108439706764281568814') {
        const p = new Promise(function (resolve, reject) {
            const request = new XMLHttpRequest();
            request.open('GET', 'https://photo.globalbus.info/zikes/mylist');
            //request.open('GET', 'http://localhost:8080/zikes/mylist');
            request.responseType = 'json';
            request.onload = function () {
                if (request.status === 200) {
                    resolve(request.response);
                } else {
                    // If it fails, reject the promise with a error message
                    reject(Error(request.statusText));
                }
            };
            request.onerror = function () {
                reject(Error('There was a network error.'));
            };
            request.send();
        });
        p.then(parseData);
    }
    const baseLayers = {
        'OpenStreetMap H.O.T.': L.tileLayer.provider('OpenStreetMap.HOT'),
        'OpenTopoMap': L.tileLayer.provider('OpenTopoMap'),
        'Hydda Full': L.tileLayer.provider('Hydda.Full')
    };
    const overlays = {
        'Gminy': gminy
    };
    L.Map.addInitHook(function () {
        L.hash(this);
        L.setOptions(this, {"inertia": false});
        if (state.enabled) {
            this.addLayer(gminy);
        }
        const enabledSave = function (e) {
            if (e.layer === gminy) {
                state.enabled = !state.enabled;
            }
        };
        this.on('overlayadd', enabledSave);
        this.on('overlayremove', enabledSave);
        mapsPlaceholder = this;
    });
    L.Control.Layers.addInitHook(function () {
        for (let i in baseLayers) {
            this._addLayer(baseLayers[i], i);
        }
        for (let i in overlays) {
            this._addLayer(overlays[i], i, true);
        }
    });
    window.addEventListener("beforeunload", function () {
        //save data
        localStorage.setItem('gminy', JSON.stringify(state));
    }, false);
    window.setTimeout(function () {
        //$('#toolsAccordion').find('>div>div[role="tab"]>div.panel-title>div>div:nth-child(2)').remove();
        const root = $('#toolsAccordion').find('>div>div>div');
        const removeHalf = function (i, e) {
            $(e).children().slice(e.childElementCount / 2).remove();
        };
        root.find('>div.collapsed').each(removeHalf);
        root.filter('.panel-body').each(removeHalf);
    }, 5000);
    $('#mapWrap').empty().append('<div id="map"></div>');
    const scriptElement = document.createElement("script");
    scriptElement.src = "scripts/main.js";
    document.body.appendChild(scriptElement);
})();
//how to save data in zikes database
//zikes.journeys.mJourney.mRoutingPreferences.mCurrentProfile.metadata.opaque.gminy
//Object.entries(state.gminyPolygons).filter(([k,v])=>v.selected==='track').reduce((a,[k,v])=>a+k+',', '')